/*
 * HashNode.cpp
 *
 *  Created on: May 4, 2020
 *      Author: 13027
 */

#include "HashMap.hpp"
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <ctime>

HashNode::HashNode(std::string s){
	keyword = s;
	values = new std::string[100];
	valuesSize = 100;
	currSize = 0;
	srand(time(NULL));
}

HashNode::HashNode(){
	keyword = "";
	values = new std::string[100];
	valuesSize = 100;
	currSize = 0;
	srand(time(NULL));
}

HashNode::HashNode(std::string s, std::string v){
	keyword = s;
	values = new std::string[100];
	values[0] = v;
	valuesSize = 100;
	currSize = 1;
}

void HashNode::addValue(std::string v) {
	if (currSize + 1 >= valuesSize) {
		dblArray();
	}

	values[currSize] = v;
	currSize += 1;
}

void HashNode::dblArray() {
	string* oldvalues = values;
	int oldsize = valuesSize;
	valuesSize *= 2;
	values = new std::string[valuesSize];
	std::memcpy(values, oldvalues, sizeof(std::string) * oldsize);
	delete oldvalues;
}

std::string HashNode::getRandValue() {
	return values[rand() % currSize];
}
