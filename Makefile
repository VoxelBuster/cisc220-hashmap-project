SRCS=$(wildcard *.cpp)
OBJS=$(SRCS:.cpp=.o )

artifact: $(OBJS)
	g++ $(OBJS) -o $@


artifact_debug: clean
	g++ -g $(SRCS) -o $@ -Wall


clean:
	-rm -f *.o
	-rm -f *.out
	-rm artifact
	-rm artifact_debug


%.: %.cpp
	g++ -g -c $< -o $<.o


run: artifact
	./$<

